<?php
/**
 * @file
 * Views style plugin: invoice header attachment.
 */


/**
 * Views style plugin: invoice header attachment.
 */
class ps_time_invoice_plugin_style_invoice_header extends views_plugin_style {

  /**
   * Defines options for formatting the dates.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['date_format'] = array('default' => 'small');
    $options['custom_date_format'] = array('default' => '');

    return $options;
  }

  /**
   * Creates an options form for formatting the dates.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['date_format'] = array(
      '#type' => 'select',
      '#title' => t('Date format'),
      '#options' => array(
        'small' => t('Short date format') . ' ' . format_date($time, 'small'),
        'medium' => t('Medium date format') . ' ' . format_date($time, 'medium'),
        'large' => t('Long date format') . ' ' . format_date($time, 'large'),
        'custom' => t('Custom'),
      ),
      '#default_value' => isset($this->options['date_format']) ? $this->options['date_format'] : 'small',
    );

    $form['custom_date_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom date format'),
      '#description' => t('If "Custom", see the <a href="http://php.net/manual/function.date.php" target="_blank">PHP documentation for date formats</a>.'),
      '#default_value' => isset($this->options['custom_date_format']) ? $this->options['custom_date_format'] : '',
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-options-date-format' => array('custom')),
    );

  }

  /**
   * Does nothing for the query step.
   *
   * This view style doesn't actually do a query.
   */
  function query() {
    return array();
  }

  /**
   * Renders the invoice header.
   */
  function render() {
    // Read the information stored in the invoice settings variables.
    $company_info = check_plain(variable_get('ps_time_invoice_company_info', ''));
    $days_to_pay = intval(variable_get('ps_time_invoice_days_pay', 30));
    $inv_num = intval(variable_get('ps_time_invoice_number', 1));

    // Build the output.
    $info = array(
      // Left side: company information.
      'left' => array(
        t('Remit to:') => str_replace("\n", '<br />', "\n" . $company_info),
      ),
      // Right side: invoice number, invoice date, date due.
      'right' => array(
        t('Invoice number:') => $inv_num,
        t('Invoice date:') => $this->format_a_date(time()),
        t('Date due:') => $this->format_a_date(time() + 24 * 3600 * $days_to_pay),
      ),
    );

    // Save the information in the view so the preprocess theme function can
    // send it to the template.
    $this->view->invoice_header_info = $info;

    // Call the theme function.
    $output = theme($this->theme_functions(), $this->view, $this->options, NULL, $this->title);

    return $output;
  }

  /**
   * Formats a date according to the options set.
   *
   * @param $date
   *   Date to format.
   *
   * @return
   *   Formatted date using the options set for this style plugin instance.
   */
  function format_a_date($date) {
    $format = $this->options['date_format'];
    if ($format == 'custom') {
      $custom_format = $this->options['custom_date_format'];
      return format_date($date, $format, $custom_format);
    }

    return format_date($date, $format);
  }
}
