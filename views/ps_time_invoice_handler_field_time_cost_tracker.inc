<?php
/**
 * @file
 * Views field handler: calculates the time cost on a tracker entry.
 */


/**
 * Views field handler: calculates the time cost on a time tracker entry.
 *
 * Assumes the following are defined on the view (fields may be hidden):
 * - Duration Field (must be before this field).
 * - Job Relationship.
 * - Hourly rate Field on Job (must be before this field).
 */
class ps_time_invoice_handler_field_time_cost_tracker extends ps_time_invoice_handler_field_time_cost_node {

  /**
   * Adds this field to the query.
   */
  function query() {
    $this->ensure_my_table();

    // Get table aliases for the dependent fields.
    $time_alias = $this->find_field_table('duration');
    $rate_alias = $this->find_field_table('field_ps_rate_value');

    // Add field to the query to calculate the value, noting that some of the
    // input fields may be NULL, so we need to check and use 0 in the sum if so.
    // The suggested field alias is 'time_cost'.
    $this->field_alias = $this->query->add_field(NULL,
      "$rate_alias.field_ps_rate_value * $time_alias.duration",
      'time_cost');
  }

}
