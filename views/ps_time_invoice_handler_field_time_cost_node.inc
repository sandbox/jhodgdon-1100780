<?php
/**
 * @file
 * Views field handler: calculates the time cost on a Business Activity node.
 */


/**
 * Views field handler: calculates the time cost on a Business Activity node.
 *
 * Assumes the following are defined on the view (fields may be hidden):
 * - Total time Field (must be before this field).
 * - Job Relationship.
 * - Hourly rate Field on Job (must be before this field).
 */
class ps_time_invoice_handler_field_time_cost_node extends views_handler_field_numeric {

  /**
   * Adds this field to the query.
   */
  function query() {
    $this->ensure_my_table();

    // Get table aliases for the dependent fields.
    $time_alias = $this->find_field_table('total_time');
    $rate_alias = $this->find_field_table('field_ps_rate_value');

    // Add a calculated field to the query to calculate the time cost. The
    // suggested field alias is 'time_cost'.
    $this->field_alias = $this->query->add_field(NULL,
      "$rate_alias.field_ps_rate_value * $time_alias.total_time",
      'time_cost');
  }

  /**
   * Finds the table alias for a field already added to the view.
   *
   * This will only work if the fields have been added and they are unique, so
   * it is a bit of a hack. However, it works.
   *
   * @param $fieldname
   *   Machine name of the field we are looking for.
   *
   * @return
   *   Alias for the table this field is part of.
   */
  function find_field_table($fieldname) {
    // Go through the list of all fields currently on the query until we
    // find one with the right name.
    foreach ($this->query->fields as $info) {
      if ($info['field'] == $fieldname) {
        // When we find it, return the table it's on.
        return $info['table'];
      }
    }
  }
}
