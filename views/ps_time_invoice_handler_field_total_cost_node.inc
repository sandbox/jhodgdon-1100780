<?php
/**
 * @file
 * Views field handler: calculates the total cost on a Business Activity node.
 */


/**
 * Views field handler: calculates the total cost on a Business Activity node.
 *
 * Assumes the following are defined on the view (fields may be hidden):
 * - Billable amount Field (must be before this field).
 * - Total time Field (must be before this field).
 * - Job Relationship.
 * - Hourly rate Field on Job (must be before this field).
 */
class ps_time_invoice_handler_field_total_cost_node extends ps_time_invoice_handler_field_time_cost_node {

  /**
   * Adds this field to the query.
   */
  function query() {
    $this->ensure_my_table();

    // Get table aliases for our dependent fields.
    $time_alias = $this->find_field_table('total_time');
    $rate_alias = $this->find_field_table('field_ps_rate_value');
    $amt_alias = $this->find_field_table('field_ps_billable_amount_value');

    // Add a field to the query that does the calculation for total cost,
    // noting that some of the fields may be NULL, so we need to check and
    // use 0 in the sum if so. The suggested field alias is 'total_cost'.
    $this->field_alias = $this->query->add_field(NULL,
      "IF ($amt_alias.field_ps_billable_amount_value IS NULL, 0, $amt_alias.field_ps_billable_amount_value) +" .
      "( IF ($rate_alias.field_ps_rate_value IS NULL, 0, $rate_alias.field_ps_rate_value) * " .
      " IF ($time_alias.total_time IS NULL, 0, $time_alias.total_time))",
      'total_cost');
  }

}
