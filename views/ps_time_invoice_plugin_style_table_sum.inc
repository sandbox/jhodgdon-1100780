<?php
/**
 * @file
 * Views style plugin: table with sums of desired columns.
 */


/**
 * Views style plugin: table with sums of desired columns.
 *
 * Somewhat borrowed from the Views Calc module, but simpler and hopefully
 * more reliable.
 */
class ps_time_invoice_plugin_style_table_sum extends views_plugin_style_table {

  /**
   * Defines options to determine which columns are summed.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['which_cols'] = array('default' => '');
    return $options;
  }

  /**
   * Returns an options form to determine which columns are summed.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['which_cols'] = array(
      '#type' => 'textfield',
      '#title' => t('Which columns to sum'),
      '#default_value' => $this->options['which_cols'],
      '#description' => t('Enter a comma-separated list of which columns to sum, giving their column labels.'),
    );
  }

  /**
   * Renders the table with the subtotal/total rows.
   *
   * This overrides the default table rendering, and is borrowed heavily
   * from views_plugin_style_table::render().
   */
  function render() {
    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Figure out which columns we need to sum. The user has entered labels
    // here. We need to figure out what the column labels are.
    $tmp_labels = explode(',', check_plain($this->options['which_cols']));
    $col_labels = array();
    foreach ($tmp_labels as $label) {
      $col_labels[] = trim($label);
    }

    // And then from those column labels, find the field aliases and field
    // names. Views uses aliases sometimes and field names sometimes, so we
    // need to keep track of both so we can use them when we need them.
    $fields = $this->view->field;
    $all_cols = $this->sanitize_columns($this->options['columns'], $fields);
    $cols = array();
    $init_totals = array();
    foreach ($all_cols as $field => $column) {
      // See if this is a field we need to sum.
      if ($field == $column && empty($fields[$field]->options['exclude']) && in_array($fields[$field]->options['label'], $col_labels)) {
        // Save the information we need, and initialize the totals.
        $alias = $fields[$field]->field_alias;
        $cols[$alias] = $field;
        $init_totals[$alias] = 0;
      }
    }

    // Calculate the totals for each "set" (i.e., group).
    $output = '';
    $group_index = 0;
    $last_group_index = count($sets) - 1;
    $grand_total = $init_totals;
    foreach ($sets as $title => $results) {
      // Calculate totals
      $group_total = $init_totals;
      foreach ($results as $row) {
        foreach ($cols as $alias => $field) {
          if (isset($row->$alias)) {
            $group_total[$alias] += $row->$alias;
          }
        }
      }

      // Add group totals to the grand total, and format each number in the
      // group totals, according to the corresponding field's formatter.
      $group_totals_formatted = array();
      // The formatter function assumes it has a row object as input, so we
      // have to fake it.
      $group_total_obj = (object) $group_total;
      foreach ($cols as $alias => $field) {
        // Add the number from this column in this group to the grand totals.
        $grand_total[$alias] += $group_total[$alias];
        // Call the field's formatter for this column to format it.
        $group_totals_formatted[$field] = $fields[$field]->theme($group_total_obj);
      }

      // Save this on the view so we can theme it, but omit subtotal if there
      // is only a single group.
      if ($last_group_index != 0) {
        $this->view->group_total = $group_totals_formatted;
      }

      // If this is the last group, include a line for the grand totals
      // (formatted).
      if ($group_index == $last_group_index) {
        $grand_totals_formatted = array();
        // The formatter function assumes it has a row object as input, so we
        // have to fake it.
        $grand_total_obj = (object) $grand_total;
        foreach ($cols as $alias => $field) {
          // Call the field's formatter for this column to format it.
          $grand_totals_formatted[$field] = $fields[$field]->theme($grand_total_obj);
        }

        // Save this on the view so we can theme it.
        $this->view->grand_total = $grand_totals_formatted;
      }

      // Finally, pass all this information to the theme function, to render
      // this group as a table with total/subtotal lines.
      $output .= theme($this->theme_functions(), $this->view, $this->options, $results, $title);
      $group_index++;
    }
    unset($this->view->row_index);
    return $output;
  }
}
