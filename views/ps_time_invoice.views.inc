<?php

/**
 * @file
 * Views integration for PS Time and Invoice module.
 */

drupal_add_css(drupal_get_path('module', 'ps_time_invoice') . '/views/ps_time_invoice.css');

/**
 * Implements hook_views_data().
 *
 * Defines the following tables/fields for PS Time and Invoicing views:
 * - Table ps_time_invoice: Aggregates time_tracker time data to calculate
 *   total time and allow this to be used in a node view (field: total_time).
 * - Table node: Adds fields 'time_cost' (hourly rate * total time, requires
 *   a Job relationship), 'total_cost' (time_cost + billable amount). Both of
 *   these are calculated "fields", and may not work in all Views plugins. For
 *   instance, Views Calc's table with calculations doesn't like them.
 * - Table time_tracker_entry: Adds field 'time_cost' (time from a tracker line
 *   item multiplied by the hourly rate, requires a Job relationship). This is
 *   a calculated "fields", and may not work in all Views plugins.
 */
function ps_time_invoice_views_data() {
  $data = array();

  // Define a "table" called 'ps_time_invoice', which will actually be
  // a subquery that will add up all the time tracked on the node from the
  // Time Tracker module.
  $data['ps_time_invoice'] = array(
    'table' => array(
      // Put all fields here into the Time and Invoicing group in the fields
      // user interfaces.
      'group' => t('Time and invoicing'),
      // Join this "table" to the Node table, so this can be used in node views,
      // which is the whole point.
      'join' => array(
        'node' => array(
          'left_field' => 'nid',
          'field' => 'nid',
          // Use a custom join handler, which constructs the query. This is
          // defined below.
          'handler' => 'ps_time_invoice_join_handler',
        ),
      ),
    ),

    // Create a field called total_time, to add up all the time tracked on
    // a node. Allow it to be used as a numeric Field, Filter, and Sort.
    'total_time' => array(
      'title' => t('Total time'),
      'help' => t('The total time tracked on the node (not including time tracked on comments)'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  // Add some fields for 'node' table views (although they are actually
  // on a mix of tables).
  $data['node'] = array(

    // Create a field called time_cost, which figures out the hourly rate
    // from the job (requires a Job relationship) and multiplies it by the total
    // time from the ps_time_invoice table above. Note that this field only
    // works if the required fields noted below are defined before it in the
    // field list of the view.
    'time_cost' => array(
      'title' => t('Time cost'),
      // Put this into the group above.
      'group' => t('Time and invoicing'),
      'help' => t('Time tracked multiplied by hourly rate on Business Activity. Requires: Job node reference Relationship, Billing rate Field from Job, and Total time Field (fields must be before this one in the field list).'),
      'field' => array(
        'handler' => 'ps_time_invoice_handler_field_time_cost_node',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),

    // Create a field adds the time_cost field from above to the Billable amount
    // field. Note that this field only works if the required fields noted
    // below are defined before it in the  field list of the view.
    'total_cost' => array(
      'title' => t('Total cost'),
      // Put this into the group above.
      'group' => t('Time and invoicing'),
      'help' => t('Total time multiplied by billing rate plus billable amount for Business Activity. Requires: Job node reference Relationship, Total time Field, Billing rate Field on Job, Billable amount Field (fields must be defined before this one in the field list).'),
      'field' => array(
        'handler' => 'ps_time_invoice_handler_field_total_cost_node',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  // Add a field for 'time_tracker_entry' table views (although it is
  // actually on a mix of tables).
  $data['time_tracker_entry'] = array(
    // Create a field called time_cost, which figures out the hourly rate
    // from the Job (requires a Job relationship) and multiplies it by the time.
    'time_cost' => array(
      'title' => t('Time cost for Tracker'),
      // Put this into the group above
      'group' => t('Time and invoicing'),
      'help' => t('Time tracked multiplied by hourly rate on Tracker entry. Requires: Job node reference Relationship, Billing rate Field on Job, Duration Field (fields must be defined before this one in the field list).'),
      'field' => array(
        'handler' => 'ps_time_invoice_handler_field_time_cost_tracker',
        'click sortable' => TRUE,
        'float' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  return $data;
}

/**
 * Implements hook_views_handlers().
 *
 * Defines field handlers for the Time cost on Node, Time cost on Time Tracker,
 * and Total cost on Node fields.
 */
function ps_time_invoice_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ps_time_invoice') . '/views',
    ),
    'handlers' => array(
      'ps_time_invoice_handler_field_time_cost_node' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'ps_time_invoice_handler_field_time_cost_tracker' => array(
        'parent' => 'ps_time_invoice_handler_field_time_cost_node',
      ),
      'ps_time_invoice_handler_field_total_cost_node' => array(
        'parent' => 'ps_time_invoice_handler_field_time_cost_node',
      ),
    ),
  );
}

/**
 * Implements hook_views_plugins().
 *
 * Defines the style plugin for Table with column sums views style, and the
 * Invoice header style.
 */
function ps_time_invoice_views_plugins() {
  return array(
    'style' => array(
      // Information for the Table with Column Sums style plugin.
      'ps_table_sum' => array(
        'title' => t('Table with column sums'),
        'help' => t('Creates a table, with a row below that contains the sum of the desired columns, with subtotals on each group if it is grouped'),
        'handler' => 'ps_time_invoice_plugin_style_table_sum',
        'path' => drupal_get_path('module', 'ps_time_invoice') . '/views',
        'parent' => 'table',
        'theme' => 'ps_table_sum',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),

      // Information for the Invoice Header style plugin.
      'ps_invoice_header' => array(
        'title' => t('Invoice header'),
        'help' => t('Header for an invoice, normally an attachment'),
        'handler' => 'ps_time_invoice_plugin_style_invoice_header',
        'path' => drupal_get_path('module', 'ps_time_invoice') . '/views',
        'theme' => 'ps_invoice_header',
        'uses row plugin' => FALSE,
        'uses fields' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );

}

/**
 * Handles the join between the node table and the time information.
 *
 * Note that this handler is in the views.inc file rather than in its own file,
 * because we need to load it. Views doesn't auto-load join handlers, unlike
 * other handlers (i.e., join handlers are not Views handlers per se, despite
 * the name).
 */
class ps_time_invoice_join_handler extends views_join {

  /**
   * Overrides the views_join::join() method.
   *
   * Custom join that makes a sub-query.
   */
  function join($table, &$query) {
    $alias = $table['alias'];
    $left = $query->get_table_info('node');
    $left_field = $left['alias'] . ".nid";

    $output = $this->type . " JOIN (SELECT SUM(`duration`) as total_time, `nid` FROM {time_tracker_entry} GROUP BY nid) as $alias ON $left_field = $alias.nid";
    return $output;
  }
}
