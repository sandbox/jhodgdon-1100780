<?php
/**
 * @file
 * Template to display an invoice header attachment.
 *
 * - $title : The title of this attachment, if any.
 * - $info: The information to display. This is an array of information by
 *   "region" (typically 'left' and 'right'). Within that, each region is an
 *   array of 'label' => 'content'.
 */
if (empty($info)) {
  print "no info";
  return;
}
?>
<div class="ps-invoice-header">
<?php if (!empty($title)) : ?>
  <h3 class="ps-invoice-header-title"><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($info as $region => $subinfo): ?>
  <div class="ps-invoice-header-<?php print $region ?>">
  <?php foreach ($subinfo as $label => $markup) : ?>
     <p class="ps-invoice-header-info"><span class="ps-invoice-header-label"><?php print $label; ?></span> <?php print $markup; ?></p>
  <?php endforeach; ?>
  </div>
<?php endforeach; ?>
</div>
<div class="ps-clear-both"></div>

