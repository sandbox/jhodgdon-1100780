PS Time and Invoice Module README.txt file

SUMMARY

This module (along with its dependencies) allows you to track time and costs for
a typical freelance Drupal shop. This is done via three content types (Client,
Job, and Business Activity). Once you have created Clients, Jobs, and Business
Activities, and entered billable costs and/or billable time, you can generate
invoices. Invoices can be printed from the browser (or printed to a PDF file,
if your browser and operating system support this). It does not support letting
your clients see the invoices on the web, because only an invoice administrator
can see the invoice that the admin is building, and only one can be built at a
time.

This module also includes working and fully documented examples of several types
of Views plugins/handlers: field definitions/handlers, join handler, and style
plugins. And it includes code to set up Flags, and programmatically flag and
unflag some nodes. So it might be useful as an example, even if you don't need
time tracking and invoicing.


INSTALLING AND SETUP

This module depends on the following modules:
- CCK (and its Node Reference, Number, Option Widgets, and Text)
- Date (and its Date API, Date Timezone)
- Flag
- Menu (core Drupal)
- Time Tracker (which depends on Views Calc, although it is not actually used
  directly by PS Time and Invoice)
- Views -- 6.x-2.x
- Views Bulk Operations

To install and set up this module:
1. Install and enable the module in the usual way. I suggest installing it
   on your local machine or local network, in its own Drupal installation,
   rather than on a machine that others can log into or see. Besides the modules
   required by this module, you may also want to enable Time Tracker Timer,
   Views UI, and Context (which requires CTools).
2. Set up a role for people who will be doing invoicing on this site.
3. On the Flags admin page (Admin > Site building > Flags), grant permission to
   the invoicing role to flag nodes as "Invoiced" and "To invoice".
4. On the Permissions admin page (Admin > User management > Permissions), grant
   permission "administer time and invoices" to this same role, as well as
   permission to create and edit content (see content type list below). If you
   have other users who are tracking their time, you will want to grant
   "add time tracker entries", "view time tracker entries", and
   "manually enter time" to them as well as to the invoicing role.
5. Using the Context module (or you could use the Blocks administration page if
   you have defined appropriate Path Auto defaults for the content types), add
   the Activities by Client and Jobs by Client blocks to Client node pages, and
   the Activities by Job block to Jobs node pages. This module does not define
   these Contexts or Block placements, because the region to put the block in is
   highly dependent on your theme. Sorry.
6. You may also want to display the Time and Invoicing menu on a sidebar, and
   the Your Active Timers block.
7. Set up content (clients etc. -- see below).
8. Set up invoicing (see below).


KNOWN ISSUES

For some reason, many of the Views pages with exposed filters come up with no
entries displayed or fewer than expected when you first visit the page. If that
happens to you, just click the "Apply" button to make Views re-query the
page. This applies to the Business activities page, the Time and overhead
report, etc.


CONTENT TYPES: CLIENTS, JOBS, AND BUSINESS ACTIVITIES

A Client is someone that you bill for services, or it can be an internal
"client" to track your overhead time and expenses.

A Job for a particular Client corresponds to a particular billing rate and can
contain a purchase order number. Note that if you have different billing rates
for different staff people or for different types of work you are doing for a
particular client, each one will need to have a separate Job, since each Job has
only one billing rate.

A Business Activity for a particular Job can be billable (with billable costs
and time for a particular invoicing period), or it can be non-billable
(representing overhead and non-billable costs for a particular tracking
period). Invoices and reports are based on Business Activities, so you need to
have separate business activities for each billing or reporting period. Each
business activity can have separate time tracking lines associated with it, but
only the total time will be shown on the Invoice Summary (see Invoicing section
below). So, you may want to use a separate Business Activity for each day's time
on a Job, in order to show daily details of hours on your Invoice Summary.

Example : For client ABC, you might have a Job called "Phase 1 Theming", and
Business Activities such as "Phase 1 Theming - January", etc.

The default content types and reports set up in this module allow you to enter
miles driven, parking and other transportation costs (bus fare, etc.), and meals
and entertainment expenses for each Business Activity. If your business has
additional tracking needs associated with business activities, you should be
able to add them as fields to the Business Activity content type and then add
those fields to the report view.

Note: The Time Tracker module also has a concept called "Activity" (each time
tracking entry can be marked with a particular Activity type). The reports
and invoices in the PS Time and Invoice module do not use or display Activities,
but you could probably modify them to use Activities if you wish.


TRACKING TIME

Time is tracked on individual Business Activites, using the Time Tracker module
and its optional timer. To track time, beging by finding a Business Activity and
opening its page in your browser. To use the timer, click "Start Timer". When
you stop the timer, enter the additional information and notes. If you prefer
not to use the timer, just scroll down and enter the time tracking line items
directly on the Business Activity page. In either case, the Time Tracker module
will keep track of all your time periods for that Business Activity.


INVOICING

To set up invoicing, visit Admin > Site configuration > Invoice settings (path
admin/settings/ps-invoice-settings). Here you can enter your company information
and the starting invoice number.

To make an invoice:

1. Decide which Client you are going to invoice.

2. Select one or more Business Activities for this Client to include on the
   invoice, and mark (flag) them as items to include on the next invoice. The
   most convenient way is probably from the Client page, where you can see a
   list of un-invoiced Business Activities, check them off, and use the Bulk
   Operations to mark them for the next invoice.

3. Visit path ps-invoice on your site to generate a summary invoice, or path
   ps-invoice-detail to generate an hours detail invoice. The summary invoice
   has one line item for each Business Activity, and is grouped and subtotaled
   by Job. It includes both billable time and billable amounts. The hours detail
   breaks out the billable time line items, and is grouped by Business Activity
   (but it does not include billable amounts, only time).

4. When you have printed or saved your invoice summary and/or detail, visit
   admin/settings/ps-invoice-finalize (which you can reach from Admin > Site
   configuration > Invoice settings) to finalize the invoice. This will mark the
   Business Activities you just used as "invoiced", and increment the invoice
   number (after asking you to confirm).


VIEWS AND REPORTS

This module provides the following views pages:

Client list (path ps-clients) - Filterable view page that finds clients you have
created.

Activities list (path ps-activities) - Filterable view page that finds Business
Activities with various properties, and performs bulk operations on them (such
as marking them for the next invoice).

Time and Overhead report (path ps-overhead-detail) - Filterable view page that
adds up time, miles driven, transportation overhead, and meals and entertainment
for a time period of your choice.

Client item page - has Views blocks that show the Jobs and un-invoiced Business
Activities associated with the Client, and let you perform bulk operations on
the Business Activities.

Job item page - has a Views block that shows the un-invoiced Business Activities
associated with the Job, and lets you perform bulk operations on the Business
Activities.
