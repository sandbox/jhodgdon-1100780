<?php
/**
 * @file
 * Template to display a view as a table with totals of selected columns.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $rows: An array of row items. Each row is an array of content keyed by
 *   field ID.
 * - $subtotal_row: The calculated and formatted subtotal row for this group.
 * - $total_row: The calculated overall total row, if it should be displayed.
 */
if (empty($rows)) {
  return;
}
?>
<table class="<?php print $class; ?>">
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th class="views-field views-field-<?php print $fields[$field]; ?>">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $count => $row): ?>
      <tr class="<?php print ($count % 2 == 0) ? 'even' : 'odd';?>">
        <?php foreach ($row as $field => $content): ?>
          <td class="views-field views-field-<?php print $fields[$field]; ?>">
             <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot>
  <?php if (isset($subtotal_row)) : ?>
      <tr class="view-groupfooter-number">
        <?php foreach ($subtotal_row as $field => $content): ?>
          <td class="views-field views-field-<?php print $fields[$field]; ?>">
             <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
  <?php endif; ?>
  <?php if (isset($total_row)) : ?>
      <tr class="view-groupfooter-number">
        <?php foreach ($total_row as $field => $content): ?>
          <td class="views-field views-field-<?php print $fields[$field]; ?>">
             <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
  <?php endif; ?>
  </tfoot>
</table>
